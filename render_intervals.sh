#!/bin/bash

usage() { echo "Useage: $0 [-m <MINUTES>] <FILE>"; exit; }

while getopts "c:m:" opt; do
  case $opt in
    m)
      WINDOW=$OPTARG
      ;;
    \?)
      usage
      ;;
  esac
done
shift $(($OPTIND-1))

FILE=$1

if [ -z $WINDOW ]; then
  WINDOW=5
fi

(( WINDOW_SECS = $WINDOW * 60 ))

# Validate that a file was passed
if [ -z "$FILE" ]
then
  echo "Usage: $0 <FILE>"
  exit
fi

# Verify that jq is available
jq --version > /dev/null
rc=$?

if [ $rc != 0 ]
then
  echo "jq not found"
  exit
fi

top_by_chunks=`jq -s 'def gettime: explode | (.[20:23] | implode | tonumber / 1000) as $frac | .[0:19] | implode | . + "Z" | fromdate | . + $frac; \
  def starttime: .[0].time | gettime; \
  def add_egroup($s): ((.time | (gettime - $s) / '$WINDOW_SECS') | floor) as $egrp | . + {egroup: $egrp}; \
  def add_egroup_time($et): . + {etime: $et}; \
  def perc($i): sort_by(.duration) | ($i * length | floor) as $idx | .[$idx].duration; \
  def perc_d($i): sort_by(.db) | ($i * length | floor) as $idx | .[$idx].db; \
  def perc_vw($i): sort_by(.view) | ($i * length | floor) as $idx | .[$idx].view; \
  def total_dr: map(.duration) | add; \
  def total_vw: map(.view) | add; \
  def total_d: map(.db) | add; \
  def max_dr: map(.duration) | max; \
  def max_vw: map(.view) | max; \
  def max_d: map(.db) | max; \
  def dur_stats: . | {batch: .[0].etime, count: length, total_dur: total_dr, perc_dur: perc(0.99), perc_view: perc_vw(0.99), perc_db: perc_d(0.99), max_dur: max_dr, max_view: max_vw, max_db: max_d, total_view: total_vw, total_db: total_d };
  starttime as $st \
  | map(add_egroup($st)) \
  | group_by(.egroup) \
  | .[] \
  | .[0].time as $etime \
  | map(add_egroup_time($etime)) \
  | dur_stats \
  | "\(.batch) \(.count) \(.total_dur | round) \(.perc_dur) \(.perc_view) \(.perc_db) \(.max_dur) \(.max_view) \(.max_db) \(.total_view / .total_dur * 100 | round) \(.total_db / .total_dur * 100 | round)"' $FILE \
  | tr -d '"' \
  | awk 'BEGIN { printf "Ratio of time spent in DB and VIEW  per '$WINDOW' Minute Span\n%-24s     %8s     %8s     %8s     %8s     %8s     %8s     %8s     %8s     %9s     %7s\n", "TIME", "COUNT", "TOTAL_DUR", "DUR_P99", "VIEW_P99", "DB_P99", "MAX_DUR", "MAX_VIEW", "MAX_DB", "PERC_VIEW", "PERC_DB"}; { printf "%-24s     %8i   %8i ms  %8.2f ms  %8.2f ms  %8s ms  %8.2f ms  %8.2f ms  %8.2f ms    %9i%%    %7i%%\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11; }
  END { printf "\r\n" }'`

echo "$top_by_chunks"
